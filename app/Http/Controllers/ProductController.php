<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function allProduct(Request $request){
        $products = Product::all();
        return json_encode($products);
    }

    public function product($id){
        $product = Product::find($id);
        return json_encode($product);
    }

    public function createProduct(Request $request){
        return json_encode(Product::create($request->all()));
    }
}
